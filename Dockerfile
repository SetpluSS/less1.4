FROM debian:9 AS build

RUN apt update &&\
    apt install -y git wget gcc make zlib1g-dev libpcre++-dev zlib1g-dev lua5.3 liblua5.3-0 liblua5.3-dev

WORKDIR /opt

RUN git clone https://github.com/openresty/luajit2.git && \
    cd luajit2 && make && make install && cd /opt && \
    git clone https://github.com/openresty/lua-resty-core.git && \
    cd lua-resty-core && make install && cd /opt && \
    git clone https://github.com/openresty/lua-resty-lrucache.git && \
    cd lua-resty-lrucache && make install && cd /opt && \
    git clone https://github.com/vision5/ngx_devel_kit.git && \
    git clone https://github.com/openresty/lua-nginx-module.git && \
    wget https://nginx.org/download/nginx-1.18.0.tar.gz &&\
    tar xvzf nginx-1.18.0.tar.gz && cd /opt/nginx-1.18.0 && \
    export LUAJIT_LIB=/usr/local/lib && \
    export LUAJIT_INC=/usr/local/include/luajit-2.1 && \
    ./configure --prefix=/opt/nginx --with-ld-opt="-Wl,-rpath,/usr/local/lib" --add-module=/opt/ngx_devel_kit --add-module=/opt/lua-nginx-module &&\
    make && make install

FROM debian:9
WORKDIR /opt
COPY --from=build /opt/nginx ./nginx
COPY --from=build /usr/local/lib /usr/local/lib
RUN chmod +x /opt/nginx/sbin/nginx
# RUN mkdir ../logs ../conf && touch ../logs/error.log

CMD ["/opt/nginx/sbin/nginx", "-g", "daemon off;"]
